const matrix = require('rpi-ws281x-native');
const pixelCount = 900;
matrix.init(pixelCount);

const sleep = (ms) => {
    return new Promise(resolve => setTimeout(resolve, ms));
}

(async () => {
    for(let i = 0; i<=pixelCount; i++){
        const leds = new Uint32Array(pixelCount);
        leds[i] = 0xffffff
        matrix.render(leds);
        console.log(i);
        await sleep(150);
    }
})();
const { spawn } = require('child_process');
const fs = require('fs');

module.exports = class StripClass {
    constructor() {
        this.ledCount = 900;
        this.leds = new Uint32Array(this.ledCount);
        this.matrix = require('rpi-ws281x-native');
        this.matrix.init(this.ledCount);

        // init the mqtt manager
        this.mqttManager = require('./lib/mqtt');
        this.mqttManager.init();

        // set the effects
        this.effectProcess = false;

        // set initial messages
        this.setRGB('255,255,255', false);
        this.setBrightness('255', false);
        this.setState('OFF');
        this.setEffect('none')

        this.mqttManager.client.on('message', (topic, message) => {
            console.log('received mqtt message on topic', topic, 'message:', message.toString())

            if(topic === this.mqttManager.listening.STATE){
                this.setState(message.toString());
            }

            if(topic === this.mqttManager.listening.SET_EFFECT){
                this.setEffect(message.toString())
            }

            if(topic === this.mqttManager.listening.SET_RGB){
                this.setRGB(message.toString())
            }

            if(topic === this.mqttManager.listening.SET_BRIGHTNESS){
                this.setBrightness(message.toString())
            }
        });

    }

    setBrightness(message, render = true){
        console.log('setBrightness', render);
        this.killEffect();
        this.brightness = parseInt(message);
        this.mqttManager.sendMessage(this.mqttManager.status.BRIGHTNESS, this.brightness.toString());
        if(render){
            this.matrix.setBrightness(this.brightness);
            this.matrix.render(
                this.leds
            );
        }
    }

    setRGB(message, render = true){
        console.log('setRGB', render)
        this.killEffect();
        let aryColors = message.split(',')
        this.rgb = [aryColors[0], aryColors[1], aryColors[2]];
        this.mqttManager.sendMessage(this.mqttManager.status.RGB, message);
        this.leds.fill(this.rgbToColor.apply(this, this.rgb));
        if(render){
            this.matrix.render(
                this.leds
            );
        }
    }

    setState(message){
        if(this.state !== message){
            console.log('setState', message);
            this.killEffect();
            this.state = message;
            this.mqttManager.sendMessage(this.mqttManager.status.STATE, message);
            if(this.state === this.mqttManager.state.OFF){
                let offLeds = new Uint32Array(this.ledCount)
                this.matrix.render(
                    offLeds
                );
            }else if(this.state === this.mqttManager.state.ON){
                this.matrix.render(
                    this.leds
                );
            }
        }
    }

    setEffect(effect, params = []){
        console.log('setEffect', effect);
        this.killEffect();

        // does the effect file exist?
        let effectFile = '/opt/rpi-strip-lights/effects/'+effect+'.js';
        try {
            if(fs.existsSync(effectFile)) {
                console.log("The file exists.");
            } else {
                effectFile = false
            }
        } catch (err) {
            effectFile = false
            console.error(err);
        }
        console.log('effect file', effectFile);

        if(effectFile !== false){
            this.mqttManager.sendMessage(this.mqttManager.status.EFFECT, effect);
            this.effectProcess = spawn('node', [effectFile, this.brightness]);
        }else{
            if(this.state === this.mqttManager.state.ON){
                this.matrix.render(
                    this.leds
                );
            }
        }

    }

    rgbToColor(r,g,b){
        return (g << 16) | (r << 8)| b;
    }

    killEffect(){
        if(this.effectProcess){
            this.effectProcess.kill('SIGINT');
            this.effectProcess = false;
            this.setEffect('none')
            this.mqttManager.sendMessage(this.mqttManager.status.EFFECT, 'none');
        }
    }
}

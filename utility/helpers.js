const helper = {
    sleep : ms => {
        return new Promise((accept) => {
            setTimeout(accept, ms)
        })
    },
    rgbToColor: (r,g,b) => {
        return (r << 16) | (g << 8)| b;
    },
    randomNumber: (min, max) => {
        return Math.floor(Math.random() * (max - min) + min);
    },
    wheel: pos => {
        let rtn = 0;
        if(pos < 85) {
            rtn = helper.rgbToColor(pos * 3, 255 - pos * 3, 0)
        }else if(pos < 170){
            pos -= 85;
            rtn = helper.rgbToColor(255 - pos * 3, 0, pos * 3)
        }else{
            pos -= 170;
            rtn = helper.rgbToColor(0, pos * 3, Math.abs(255 - pos * 3));
        }
        if(rtn === 65537 || rtn === 65794){
            rtn = 65280;
        }
        return rtn;
    }
};

module.exports = helper;
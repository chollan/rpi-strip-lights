const prompt = require('prompt-async');
const ws281x = require('rpi-ws281x');
ws281x.configure({
    leds: 900,
});

prompt.start();

(async () => {
    while(true){
        try{
            let { led } = await prompt.get({
                name: 'led',
                validator: /^[0-9\-]+$/,
                warning: 'LED Number must be numbers only.'
            });
            let pixels = new Uint32Array(900);
            if(led.indexOf('-') > 0){
                let range = led.split('-');
                for(let i=parseInt(range[0]); i<=parseInt(range[1]); i++){
                    pixels[i] = 0xffffff;
                }
                console.log('turning on pixels '+range[0]+' thru '+range[1]);
            }else{
                pixels[led] = 0xffffff;
                console.log('turning on pixel '+led)
            }
            ws281x.render(pixels);
        }catch (e) {
            // turn them off
            ws281x.render(new Uint32Array(900));
            console.log('Exiting');
            process.exit();
        }
    }
})();
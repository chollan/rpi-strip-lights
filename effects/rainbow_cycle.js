const matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers')
const ledCount = 900;
const pixelCount = ledCount;
const pixels = new Uint32Array(ledCount);
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));

let colors = Array.from(Array(256).keys());
while(true){
    for(let j = 0; j<=colors.length; j++){
        for(let i = 0; i<pixelCount; i++){
            pixels[i] = helpers.wheel(((256 / pixelCount) + j) % 256)

        }
        matrix.render(pixels);
    }
}
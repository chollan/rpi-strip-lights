const matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
const ledCount = 900;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));

let previous = false;
setInterval(() => {
    let pixels = new Uint32Array(ledCount);
    for(let i=0;i<=helpers.randomNumber(0,90); i++){
        pixels[helpers.randomNumber(0,ledCount)] = 0xffffff;
    }
    matrix.render(pixels);
}, 20);

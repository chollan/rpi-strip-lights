let matrix = require('rpi-ws281x-native');
let ledCount = 900;
const helpers = require('../utility/helpers');
let red = new Uint32Array(ledCount);
let blue = new Uint32Array(ledCount);
let off = new Uint32Array(ledCount);

// fill the colors
let counter = 1;
for(let i = 0; i<=red.length; i+=1){
    if(counter <= 25){
        red[i] = 0x00ff00;
    }else if(counter > 25){
        blue[i] = 0x0000ff;
    }
    if(counter === 50){
        counter = 0;
    }else{
        counter++;
    }
}


const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));


setInterval(async() => {
    matrix.render(red);
    await helpers.sleep(80);
    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(red)
    await helpers.sleep(190);

    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(blue);
    await helpers.sleep(80);
    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(blue);
    await helpers.sleep(190);
    matrix.render(off);
    await helpers.sleep(80);
}, 940);

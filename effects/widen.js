let matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
let ledCount = 900;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));
let step = 10;
let wait = 5;
let color = helpers.rgbToColor(0,25,255);

const main = async function(){
    for(let i=0; i<=50; i++){
        let strip = new Uint32Array(900);
        for(let n=0; n<=strip.length; n++){
            if(n % i === 0){
                strip[n] = color;
            }
        }
        matrix.render(strip);
    }
    main()
};
main();

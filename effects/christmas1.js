let matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
let ledCount = 900;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));
let step = 10;
let wait = 5;

const main = (async function(){
console.log('called');
let strip = new Uint32Array(900);
for(let r = 0; r<=255; r+=step){
    let g = 255-r;
    console.log(r, g);
    for(let pixel = 0;pixel<=strip.length;pixel++){
        if(pixel % 2 === 0)
            strip[pixel] = helpers.rgbToColor(r, 0, 0);
        else
            strip[pixel] = helpers.rgbToColor(0, g, 0);
    }
    matrix.render(strip);
    await helpers.sleep(wait)
}
for(let g = 0; g<=255; g+=step){
    let r = 255-g;
    console.log(r, g);
    for(let pixel = 0;pixel<=strip.length;pixel++){
        if(pixel % 2 === 0)
            strip[pixel] = helpers.rgbToColor(r, 0, 0);
        else
            strip[pixel] = helpers.rgbToColor(0, g, 0);
    }
    matrix.render(strip);
    await helpers.sleep(wait)
}
main()
});

main()
//process.exit();

/*let section1 = new Uint32Array(900);
for(let i = 0; i<=900; i+=1){
    section1[i] = 0x00ff00;
}
let red = new Uint32Array(900);
for(let i = 1; i<=900; i+=2){
    red[i] = 0xff0000;
}
matrix.render(off);*/

let matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
let ledCount = 900;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));
let step = 10;
let wait = 5;
let pixelGroups = 6;

const main = (async function(){
    let strip = new Uint32Array(900);
    let color = helpers.rgbToColor(0,255,0);
    for(let groupingPixel=0; groupingPixel<=pixelGroups; groupingPixel++){
        let strip = new Uint32Array(900);
        let count=0;
        if(color == helpers.rgbToColor(255,0,0)){
            color = helpers.rgbToColor(0,255,0)
        }else{
            color = helpers.rgbToColor(255,0,0)
        }
        for(let pixel=0; pixel<=strip.length; pixel++){
            if(count === groupingPixel){
                if(color == helpers.rgbToColor(255,0,0)){
                    color = helpers.rgbToColor(0,255,0)
                }else{
                    color = helpers.rgbToColor(255,0,0)
                }
                strip[pixel] = color
            }
            count++;
            if(count>pixelGroups){
                count = 0;
            }
        }
        matrix.render(strip);
        await helpers.sleep(220);
    }
    main()
});
main();

const matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
const pixelCount = 900;
const pixels = new Uint32Array(pixelCount);
const brightness = process.argv[2];
matrix.init(pixelCount);
matrix.setBrightness(parseInt(brightness));


for(let i = 0; i<pixelCount; i++){
    pixels[i] = helpers.wheel(((i * 256 / pixelCount)) % 256)
}
matrix.render(pixels);

// we need this file to run forever
// so let's start a forever loop
// and this file can be killed from
// the main application
while(true){}
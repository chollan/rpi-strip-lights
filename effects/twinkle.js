let matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
let ledCount = 900;
let pixelCount = ledCount;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));

let r=0,g=0,b=255;
let random = helpers.randomNumber(0,ledCount);
let whiteColor = 150;
let mainColor = helpers.rgbToColor(r,g,b);
let waitTimer = 0;
let saidGo = false;
setInterval(async () => {
    if(waitTimer === 0){
        if(!saidGo){
            console.log('GO!');
            saidGo = true;
        }
        let pixels = new Uint32Array(ledCount);
        for(let i = 0; i<=pixelCount; i++){
            if(random === i){
                if(whiteColor > 0){
                    pixels[i] = helpers.rgbToColor(
                        r>=whiteColor?r:whiteColor,
                        g>=whiteColor?g:whiteColor,
                        b>=whiteColor?b:whiteColor
                    );
                    whiteColor -= 3;
                }else{
                    random = helpers.randomNumber(0,ledCount);
                    whiteColor = 255;
                    pixels[i] = mainColor;
                    waitTimer = helpers.randomNumber(30,300);
                    console.log('STOP!  next pixel is',random,'waiting', waitTimer, 'ticks')
                    saidGo = false;
                }
            }else{
                pixels[i] = mainColor;
            }
        }
        matrix.render(pixels);
    }else{
        waitTimer--;
    }
}, 20);
let matrix = require('rpi-ws281x-native');
let ledCount = 900;
let colors = Array.from(Array(256).keys());
let pixelCount = ledCount;
let pixels = new Uint32Array(ledCount);
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));

const wheel = pos => {
    let rtn = 0;
    if(pos < 85) {
        rtn = rgbToColor(pos * 3, 255 - pos * 3, 0)
    }else if(pos < 170){
        pos -= 85;
        rtn = rgbToColor(255 - pos * 3, 0, pos * 3)
    }else{
        pos -= 170;
        rtn = rgbToColor(0, pos * 3, Math.abs(255 - pos * 3));
    }
    if(rtn === 65537 || rtn === 65794){
        rtn = 65280;
    }
    return rtn;
};

const rgbToColor = (r,g,b) => {
    return (r << 16) | (g << 8)| b;
}

let count = 0;
while(true){
    for(let j = 0; j<colors.length; j++){
        for(let i = 0; i<pixelCount; i++){
            pixels[i] = wheel(((i * 256 / pixelCount) + j) % 256)
        }
        matrix.render(pixels);
    }
}

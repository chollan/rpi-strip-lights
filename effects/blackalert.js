const matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
const pixelCount = 900;
const pixels = new Uint32Array(pixelCount);
const brightness = process.argv[2];
matrix.init(pixelCount);
matrix.setBrightness(parseInt(brightness));

let lengthOfOn = 12;
let lengthOfOff = 24;
let counter = 0;
let off = false;
let color = helpers.rgbToColor(0,0,255);

const main = (offsetShift) => {
    counter = offsetShift;
    if(off && counter <= lengthOfOn){
        off = false;
    }
    for(let i = 0; i<pixelCount; i++){
        if(counter <= lengthOfOn){
            pixels[i] = color;
        }else if(counter <= (lengthOfOff+lengthOfOn)){
            pixels[i] = 0x000000;
        }
        counter++;
        if((lengthOfOn + lengthOfOff) <= counter){
            counter = 0;
        }
    }
    pixels.reverse(); //we want this to run up the stairs, not down
    matrix.render(pixels);

    if(offsetShift === ((lengthOfOff + lengthOfOn)-1)){
        offsetShift = 0;
    }else{
        offsetShift++;
    }
    main(offsetShift);
};
main(0);
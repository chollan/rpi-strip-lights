let matrix = require('rpi-ws281x-native');
const helpers = require('../utility/helpers');
let ledCount = 900;
const brightness = process.argv[2];

matrix.init(ledCount);
matrix.setBrightness(parseInt(brightness));

let left = new Uint32Array(900);
for(let i = 0; i<=900; i+=2){
    left[i] = 0x00ff00;
}
let right = new Uint32Array(900);
for(let i = 1; i<=900; i+=2){
    right[i] = 0x0000ff;
}
let off = new Uint32Array(900);
setInterval(async() => {
    matrix.render(left);
    await helpers.sleep(80);
    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(left)
    await helpers.sleep(190);

    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(right);
    await helpers.sleep(80);
    matrix.render(off);
    await helpers.sleep(80);
    matrix.render(right);
    await helpers.sleep(190);
    matrix.render(off);
    await helpers.sleep(80);
}, 940);
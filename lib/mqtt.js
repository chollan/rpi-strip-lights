const mqtt = require('mqtt');

module.exports = {
    client: false,
    client_ip: '192.168.1.170',
    status: {
        STATE: 'ws2812/state',
        COLOR: 'ws2812/color',
        BRIGHTNESS: 'ws2812/brightness',
        EFFECT: 'ws2812/effect',
        RGB: 'ws2812/rgb',
        /*ACTIVE_BRIGHTNESS: 'ws2812/display/activeBrightness',
        IDLE_BRIGHTNESS: 'ws2812/display/idleBrightness',
        DISPLAYING_MESSAGE: 'ws2812/display/displayingMessage',
        ERROR: 'ws2812/display/error',
        /!*DATE_FORMAT: 'rgb_screen/display/dateFormat',
        TIME_FORMAT: 'rgb_screen/display/timeFormat',*!/
        DISPLAYING_CLOCK: 'ws2812/display/clock',
        IDLE_COLOR: 'ws2812/display/idleColor',
        ATTRIBUTES: 'ws2812/display/attributes',*/
    },
    listening: {
        STATE: 'ws2812/state/set',
        SET_EFFECT: 'ws2812/effect/set',
        SET_RGB: 'ws2812/rgb/set',
        SET_BRIGHTNESS: 'ws2812/brightness/set',
        /*ACTIVE_BRIGHTNESS: 'ws2812/display/activeBrightness/set',
        IDLE_BRIGHTNESS: 'ws2812/display/idleBrightness/set',
        IDLE_COLOR: 'ws2812/display/idleColor/set',
        ATTRIBUTES: 'ws2812/display/attributes/set',*/
    },
    state:{
        OFF: 'OFF',
        ON: 'ON',
    },
    init: function(){
        this.client  = mqtt.connect('mqtt://' + this.client_ip);
        Object.values(this.listening).map((item) => {
            this.client.subscribe(item, (err) => { if (err) { console.error('error attempting to subscribe to', item, err); } });
        });
    },
    sendMessage: function(topicEdge, message){
        return new Promise((accept, reject) => {
            if(this.client){
                this.client.publish(topicEdge, message, {retain: true}, (err) => {
                    if(err)
                        return reject(err);
                    return accept();
                });
            }else{
                return reject('not connected');
            }
        });
    }
}